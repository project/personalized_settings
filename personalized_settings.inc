<?php

/**
 * @version
 *
 * @developers:
 *    Rafal Wieczorek <kenorb@gmail.com>
 */


/**
 * Convert textarea list from variable into array
 */
function personalized_settings_get_forms() {
  $forms = variable_get('personalized_settings_form_id', '');
  $form_arr = explode("\r\n", $forms);
  return array_unique($form_arr);
}

