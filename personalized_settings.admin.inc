<?php
/**
 * @file
 *   Form settings include file
 *
 * @version
 *
 * @author
 *   Rafal Wieczorek <kenorb@gmail.com>
 */


/**
 * Menu callback for the settings form.
 */
function personalized_settings_admin_form() {

  $form['personalized_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Form Settings'),
    '#collapsible' => TRUE,
  );

  $settings = variable_get('personalized_settings', array());

  $form['personalized_settings']['personalized_settings_form_id'] = array(
    '#type' => 'textarea',
    '#title' => t('Form IDs to personalize'),
    '#cols' => 60,
    '#rows' => 5,
    '#default_value' => variable_get('personalized_settings_form_id', ''),
    '#description' => t('Type which form_id you want to personalize (one per line). Example: block_admin_display_form will personalize blocks for each user.'),
    '#wysiwyg' => FALSE,
  );

  $type_options = array('per_user' => t('Per user'), 'per_role' => t('Per role')); // default: per_user
  $form['personalized_settings']['personalized_settings_personalize_type'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Settings Type'),
    '#description' => t('Select if settings should be personalized per user or per role.'),
    '#default_value' => variable_get('personalized_settings_personalize_type', array('per_user')),
    '#options' => $type_options,
  );

  /* TODO
  $form['personalized_settings']['personalized_settings_forms'] = array(
  '#type' => 'checkboxes',
  '#title' => t('Personalized Content Types'),
  '#description' => t('Select content types which will have personalized settings.'),
  '#default_value' => variable_get('personalized_settings_forms', array()),
  '#options' => node_get_types('names'), // get content types
  );
   */

  if (module_exists('userreference')) {
    $form['personalized_settings']['personalized_settings_inherit'] = array(
      '#type' => 'checkbox',
      '#title' => t('Inherit settings from referenced user.'),
      '#description' => t('Merge all personalized setting changes to all parent nodes.'),
      '#default_value' => variable_get('personalized_settings_inhired', array()),
    );
  }

  $form['personalized_settings_debug'] = array(
    '#type' => 'fieldset',
    '#title' => t('Debug settings'),
    '#description' => t('Use those settings only for testing purposes!.'),
    '#collapsible' => TRUE,
  );

  $form['personalized_settings_debug']['nodeaccess_autoreference_show_form_id'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show form id on each form.'),
    '#description' => t('Select if you want to show form id on all forms.'),
    '#default_value' => variable_get('nodeaccess_autoreference_show_form_id', FALSE),
  );

  //$form['#validate'] = array('personalized_settings_admin_form_validate');

  return system_settings_form($form);
}

/**
 * Form API callback to validate the settings form.
 */
function personalized_settings_admin_form_validate($form, &$form_state) {
  $forms = module_invoke_all('forms', $form_id,$args);
  $values = &$form_state['values'];
  $arr=explode("\n", trim($values['personalized_settings_form_id']));
  $values['personalized_settings_form_id']='';
  $forms = module_invoke_all('forms', '', $args);
  $er = '';
  foreach ($arr as $key=>$val) {
    if(strlen(trim($val))!=0) {

      $values['personalized_settings_form_id'].=trim($val,' \n')."\n";
      if( !(isset($forms[trim($val)]) || function_exists(trim($val))) ) {
        $er.=t("Form: @id -doesn't exist.",array('@id'=>trim($val)));

      }
    }
  }

  if($er!='')
    form_set_error('', $er);

} 

